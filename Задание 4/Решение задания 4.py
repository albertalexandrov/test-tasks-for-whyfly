from tariff import tariff_info

INDENT = '  '


def write_to_console(data: dict, level: int = 0) -> None:
    """
    Функция, которая выводит в консоль описание структуры данных (dict с произвольным уровнем
    вложенности) в удобочитаемом виде с отступами на каждом уровне

    Args:
        data: словарь, вывод которого необходимо отформатировать
        level: уровень вложенности

    Returns:
        None
    """

    key, values = tuple(data.items())[0]
    print(INDENT*level + key)
    level += 1

    for item in values['children']:
        if type(item) is dict:
            write_to_console(item, level)
        elif type(item) is str:
            print(INDENT*level + item)


write_to_console(tariff_info)
