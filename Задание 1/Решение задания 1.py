"""
Решение тестового задания №1

Для сохранения интерфейса работы с моделью DeliveryState (т.е. сохранения возможности написать,
к примеру, DeliveryState.get_new()) и избавления от необходимости декларативно описывать каждый метод
get_new, get_issued, чтобы таким образом избавиться от однотипного кода, был применен метакласс DeliveryStateMeta,
который на лету генерирует методы, указанные выше методы.

Данный подход можно применить при генерировании методов класса на лету. Ключевое слово "STATE" может
быть заменено на любое другое. Логика генерируемых методов может формироваться в соответствии с другими
требованиями
"""


class DeliveryStateMeta(type):
    """
    Метакласс для класса DeliveryState, который создает методы (get_new(), get_issued),
    соответствующие заданным состояниям (STATE_NEW, STATE_ISSUED и т.д.)
    """

    def __new__(mcs, name, bases, attr):
        for state, value in attr.items():
            if state.startswith('STATE_'):
                _, state_name = state.split('_', 1)
                method_name = f'get_{state_name.lower()}'

                def method(state_id):
                    return lambda _: mcs.objects.get(pk=state_id)

                setattr(mcs, method_name, method(value))

        return super(DeliveryStateMeta, mcs).__new__(mcs, name, bases, attr)


class DeliveryState(metaclass=DeliveryStateMeta):

    class Meta:
        verbose_name = u"Состояние доставки"
        verbose_name_plural = u"Состояния доставок"

    STATE_NEW = 1  # Новая
    STATE_ISSUED = 2  # Выдана курьеру
    STATE_DELIVERED = 3  # Доставлена
    STATE_HANDED = 4  # Курьер сдал
    STATE_REFUSED = 5  # Отказ
    STATE_PAID_REFUSED = 6  # Отказ с оплатой курьеру
    STATE_COMPLETE = 7  # Завершена
    STATE_NONE = 8  # Не определено
